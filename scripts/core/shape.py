from scripts.core.calculate import Calculate


class Shape:
    def ToCalculate(shap):
        if shap == 'Rectangle':
            length = int(input("Please enter the length of Rectangle : "))
            Width = int(input("Please enter the length of Rectangle : "))
            result = Calculate.Rectangle(length,Width)
            return result
        elif shap == 'Circle':
            radius = int(input("Please enter the Radius of Circle : "))
            result = Calculate.circle(radius)
            return result
        elif shap == 'Square':
            side = int(input("Please enter the side of Square : "))
            result = Calculate.Square(side)
            return result
